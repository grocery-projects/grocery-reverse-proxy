FROM haproxy:1.8-alpine
COPY haproxy.cfg /usr/local/etc/haproxy/haproxy.cfg
RUN mkdir -p /etc/ssl/certificates